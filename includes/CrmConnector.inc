<?php

/**
 * @file
 * CRM connection helpers.
 */

/**
 * Class CrmConnector.
 */
class CrmConnector {
  /**
   * CRM url to post to.
   *
   * @var apiUrl
   */
  protected $apiUrl;

  /**
   * CRM username.
   *
   * @var username
   */
  protected $username;
  /**
   * CRM password.
   *
   * @var password
   */
  protected $password;
  /**
   * Connection session.
   *
   * @var session
   */
  protected $session;

  /**
   * CrmConnector constructor.
   */
  public function __construct() {
    $this->apiUrl = variable_get('crm_url', 'https://sugarcrm-dev.private.uwaterloo.ca/service/v4_1/rest.php');
    $this->username = variable_get('crm_user', 'api');
    $this->password = variable_get('crm_pass_hash', md5('W@terl00'));
  }

  /**
   * Executes curl call.
   *
   * @param string $method
   *   Http method to use.
   * @param array $parameters
   *   Parameters to encode.
   *
   * @return string
   *   Returns decoded json response
   */
  private function execute($method, array $parameters) {
    ob_start();
    $curlRequest = curl_init();

    curl_setopt($curlRequest, CURLOPT_URL, $this->apiUrl);
    curl_setopt($curlRequest, CURLOPT_POST, 1);
    curl_setopt($curlRequest, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curlRequest, CURLOPT_HEADER, 1);
    curl_setopt($curlRequest, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curlRequest, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlRequest, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlRequest, CURLOPT_FOLLOWLOCATION, 0);

    $jsonEncodedData = json_encode($parameters);

    $post = [
      "method" => $method,
      "input_type" => "JSON",
      "response_type" => "JSON",
      "rest_data" => $jsonEncodedData,
    ];

    curl_setopt($curlRequest, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec($curlRequest);
    curl_close($curlRequest);

    $result = explode("\r\n\r\n", $result, 2);
    $response = json_decode($result[1]);
    ob_end_flush();

    return $response;
  }

  /**
   * Authorizes connection.
   *
   * @return string
   *   Returns sessionid
   */
  private function authorize() {
    $loginParameters = [
      "user_auth" => [
        "user_name" => $this->username,
        "password" => $this->password,
        "version" => "1",
      ],
      "application_name" => "RestTest",
      "name_value_list" => [],
    ];

    $loginResult = $this->execute("login", $loginParameters);
    return $this->session = $loginResult->id;
  }

  /**
   * Adds a lead.
   *
   * @param array $lead
   *   Lead.
   * @param string $campaignId
   *   CampaignId.
   * @param string $assignedId
   *   AssignedId.
   *
   * @return string
   *   Returns response.
   */
  public function addLead(array $lead, $campaignId, $assignedId) {
    $data = [
      'first_name' => check_plain($lead['first_name']),
      'last_name' => check_plain($lead['last_name']),
      'email' => check_plain($lead['email']),
      'entry_year' => check_plain($lead['entry_year']),
      'faculties' => $lead['faculties'],
      'opt_out' => check_plain($lead['opt_out']),
    ];

    $session = $this->authorize();

    $parameters = [
      // Session id.
      "session" => $session,

      // The name of the module from which to retrieve records.
      "module_name" => "Leads",

      // Record attributes.
      "name_value_list" => [
        /*
         *  To update you will nee to pass in a record id as below.
         *  array("name" => "id",
         *     "value" => "9b170af9-3080-e22b-fbc1-4fea74def88f"
         *  ),
         */
        [
          "first_name" => $data['first_name'],
          "last_name" => $data['last_name'],
          "email1" => $data['email'],
          "admitterm_c" => $data['entry_year'],
          "faculty_c" => $data['faculties'],
          "email_opt_out" => $data['opt_out'],
          "campaign_id" => $campaignId,
          "assigned_user_id" => $assignedId,
          "status" => "1prospect",
        ],
      ],
    ];

    return $this->execute("set_entries", $parameters);
  }

}
