# UW MUR CRM Lead Capture
This module is the MUR CRM lead capture customized form.

It depends on the ```uw_ct_mur_crm_lead_capture``` content type and the ```uw_crm_authorization``` module for its CRM connection.

The module provides a lead capture form that passes submissions into MUR's CRM via its REST API.
It provides a default, uncustomized lead capture form as well as the ability to have custom form *instances* that contain their 
own custom headers/footers and preset options.  These customized instances are accessable by appending a custom URL path segment 
to the default signup URL.

## Settings Page:
The settings page can be found on the dashboard side menu named ```MUR Lead Capture Form Settings```

Users with the role of ```site manager``` or ```administrator``` are able to manage these settings.

*Direct URL*: 
[uwaterloo.ca/future-students/admin/config/uw-mur-crm-lead-capture](https://uwaterloo.ca//future-students/admin/config/uw-mur-crm-lead-capture)


## Accessing the lead capture form

The default URL for the sign-up page is ```/signup```. 
Accessing this URL will use the default form settings only; set by the settings page mentioned previously.

## CRM Lead Capture Instances
A custom form instance can be created and managed by the usual content options on the WCMS workbench page.

Creating a new lead capture form instance is done by selecting the ```MUR Lead Capture``` content type and filling out the 
provided customization fields.

You can provide a custom header and footer as well as specify a *URL ID*.

The **URL ID** defines what value is used in the form's final URL.  The final URL will be ```/signup/\<URL ID>/```

**Example:**
If you set your URL ID to 'kld' (short for Kin Lab Days) your customized form URL will be:
```/signup/kld```

There are also fields for specifing CRM campaign and assigned to ids, as well as the abilty to show/hide the faculty list,
or have a particular faculty pre-selected.



